package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh04N067 {

    public static void main(String[] args) {

        System.out.println("Please enter number ( 1 <= number <= 365)");
        int day = InputReader.nextInt();
        if (day < 1 || day > 365) {
            System.out.println("Invalid input");
            return;
        }
        String result = isWWeekend(day) ? "weekend" : "working";
        System.out.println("Entered day is " + result);
    }

    public static boolean isWWeekend(int day) {
        return day % 7 == 0 || day % 6 == 0;
    }
}