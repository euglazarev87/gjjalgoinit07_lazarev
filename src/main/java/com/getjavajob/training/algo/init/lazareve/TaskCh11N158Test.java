package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh11N158.removeDuplicates;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N158Test {

    public static void main(String[] args) {
        int[] source = {0, 1, 2, 3, 4, 4, 5, 6, 7, 8, 7, 7, 9, 10};
        int[] resArr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 0, 0, 0, 0};
        assertEquals("TaskCh11N158.removeDuplicates", resArr, removeDuplicates(source));
    }
}
