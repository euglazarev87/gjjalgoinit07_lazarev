package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N054.convertIntoBinary;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N054Test {

    public static void main(String[] args) {
        assertEquals("TaskCh10N054.convertIntoBinary", "10011", convertIntoBinary(19));
    }
}
