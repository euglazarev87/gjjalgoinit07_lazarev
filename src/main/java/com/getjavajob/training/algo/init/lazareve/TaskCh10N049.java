package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh10N049 {

    public static void main(String[] args) {

        System.out.println("Please enter positive integer:");
        int n = InputReader.nextInt();
        if (n < 1) {
            System.out.println("Invalid input");
            return;
        }

        String[] array = new String[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = "Element " + (i + 1);
            System.out.println(array[i] + " with index " + i);
        }

        System.out.println();
        System.out.println("Last element is " + getLastElement(array));
    }

    public static String getLastElement(String[] arr) {
        return getLastElement(arr, 0);
    }

    private static String getLastElement(String[] arr, int iterator) {

        if (iterator == arr.length - 1) {
            return arr[iterator];
        }

        return getLastElement(arr, iterator + 1);
    }
}
