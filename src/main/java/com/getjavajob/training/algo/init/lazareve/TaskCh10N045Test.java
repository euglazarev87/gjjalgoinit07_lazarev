package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N045.*;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N045Test {

    public static void main(String[] args) {
        assertEquals("TaskCh10N045.findSumOfProgressionMembers", 30, findSumOfProgressionMembers(4, 1, 5, 0));
        assertEquals("TaskCh10N045.findValueOfMember", 7, findValueOfMember(1, 3, 3));
    }
}
