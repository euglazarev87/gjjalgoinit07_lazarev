package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;
import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh05N010.*;

public class TaskCh05N010Test {

    public static void main(String[] args) {

        String[] sample = {
                "1 USD is 71 RUB",
                "2 USD is 142 RUB",
                "3 USD is 213 RUB",
                "4 USD is 284 RUB",
                "5 USD is 355 RUB",
                "6 USD is 426 RUB",
                "7 USD is 497 RUB"
        };

        int[] range = new int[7];
        range[0] = 1;
        range[1] = 2;
        range[2] = 3;
        range[3] = 4;
        range[4] = 5;
        range[5] = 6;
        range[6] = 7;

        assertEquals("TaskCh05N010.getRatesForRange", sample, getRatesForRange(range, 71));
    }
}
