package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh10N052 {

    public static void main(String[] args) {

        System.out.println("Please enter positive integer:");
        int digit = InputReader.nextInt();
        if (digit <= 0) {
            System.out.println("Invalid input");
            return;
        }

        System.out.println("Reversed output from " + digit + " is " + getReversedOutput(digit));
    }

    public static String getReversedOutput(int n) {
        return getReversedOutput(n, new StringBuilder());
    }

    private static String getReversedOutput(int n, StringBuilder sb) {

        if (n > 0) {
            sb.append(n % 10);
            getReversedOutput(n / 10, sb);
            return sb.toString();
        }

        return "";
    }
}
