package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh11N245.getNegativeSortedArray;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N245Test {

    public static void main(String[] args) {
        int[] source = {1, 0, 2, 3, 4, 5, 6, 7, -1, -5, -3, -2, 9, 10};
        int[] resArr = {-2, -3, -5, -1, 1, 0, 2, 3, 4, 5, 6, 7, 9, 10};

        assertEquals("TaskCh11N245.getNegativeSortedArray", resArr, getNegativeSortedArray(source));
    }
}
