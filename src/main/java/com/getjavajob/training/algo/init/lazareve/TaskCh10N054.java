package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh10N054 {

    public static void main(String[] args) {

        System.out.println("Please enter positive integer: ");
        int integer = InputReader.nextInt();
        if (integer < 1) {
            System.out.println("Invalid input");
            return;
        }
        System.out.println("Integer " + integer + " is " + convertIntoBinary(integer) + " in binary number system");
    }

    public static String convertIntoBinary(int integer) {
        return convertIntoBinary(integer, new StringBuilder());
    }

    private static String convertIntoBinary(int integer, StringBuilder sb) {

        if (integer == 0) {
            return "";
        }

        convertIntoBinary(integer / 2, sb);
        sb.append(integer % 2 > 0 ? 1 : 0);

        return sb.toString();
    }
}