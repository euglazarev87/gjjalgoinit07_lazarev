package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.ArrayPrinter;

public class TaskCh12N025 {

    public static void main(String[] args) {

        System.out.println("Solved A");
        ArrayPrinter.printArray(getArrayTapeA());

        System.out.println("Solved B");
        ArrayPrinter.printArray(getArrayTypeB());

        System.out.println("Solved V");
        ArrayPrinter.printArray(getArrayTypeV());

        System.out.println("Solved G");
        ArrayPrinter.printArray(getArrayTypeG());

        System.out.println("Solved D");
        ArrayPrinter.printArray(getArrayTypeD());

        System.out.println("Solved E");
        ArrayPrinter.printArray(getArrayTypeE());

        System.out.println("Solved J");
        ArrayPrinter.printArray(getArrayTypeJ());

        System.out.println("Solved Z");
        ArrayPrinter.printArray(getArrayTypeZ());

        System.out.println("Solved I");
        ArrayPrinter.printArray(getArrayTypeI());

        System.out.println("Solved K");
        ArrayPrinter.printArray(getArrayTypeK());

        System.out.println("Solved L");
        ArrayPrinter.printArray(getArrayTypeL());

        System.out.println("Solved M");
        ArrayPrinter.printArray(getArrayTypeM());

        System.out.println("Solved N");
        ArrayPrinter.printArray(getArrayTypeN());

        System.out.println("Solved O");
        ArrayPrinter.printArray(getArrayTypeO());

        System.out.println("Solved P");
        ArrayPrinter.printArray(getArrayTypeP());
    }

    public static int[][] getArrayTapeA() {

        int[][] arr = new int[12][10];
        int value = 1;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = value;
                value++;
            }
        }
        return arr;
    }

    public static int[][] getArrayTypeB() {

        int[][] arr = new int[12][10];
        int value = 1;
        for (int j = 0; j < arr[0].length; j++) {
            for (int i = 0; i < arr.length; i++) {
                arr[i][j] = value;
                value++;
            }
        }
        return arr;
    }

    public static int[][] getArrayTypeV() {
        int[][] arr = new int[12][10];
        int value = 1;
        for (int i = 0; i < arr.length; i++) {
            for (int j = arr[i].length - 1; j >= 0; j--) {
                arr[i][j] = value;
                value++;
            }
        }
        return arr;
    }

    public static int[][] getArrayTypeG() {
        int[][] arr = new int[12][10];
        int value = 1;
        for (int j = 0; j < arr[0].length; j++) {

            for (int i = arr.length - 1; i >= 0; i--) {
                arr[i][j] = value;
                value++;
            }
        }
        return arr;
    }

    public static int[][] getArrayTypeD() {

        int[][] arr = new int[10][12];
        boolean forward = true;
        int value = 1;

        for (int i = 0; i < arr.length; i++) {

            if (forward) {
                for (int j = 0; j < arr[i].length; j++) {
                    arr[i][j] = value;
                    value++;
                }
            } else {
                for (int j = arr[i].length - 1; j >= 0; j--) {
                    arr[i][j] = value;
                    value++;
                }
            }
            forward = !forward;
        }
        return arr;
    }

    public static int[][] getArrayTypeE() {

        int[][] arr = new int[12][10];
        boolean fromTop = true;
        int value = 1;
        for (int j = 0; j < arr[0].length; j++) {

            if (fromTop) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i][j] = value;
                    value++;
                }
            } else {
                for (int i = arr.length - 1; i >= 0; i--) {
                    arr[i][j] = value;
                    value++;
                }
            }
            fromTop = !fromTop;
        }
        return arr;
    }

    public static int[][] getArrayTypeJ() {

        int[][] arr = new int[12][10];
        int value = 1;
        for (int i = arr.length - 1; i >= 0; i--) {

            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = value;
                value++;
            }
        }
        return arr;
    }

    public static int[][] getArrayTypeZ() {

        int[][] arr = new int[12][10];
        int value = 1;

        for (int j = arr[0].length - 1; j >= 0; j--) {
            for (int i = 0; i < arr.length; i++) {
                arr[i][j] = value++;
            }
        }
        return arr;
    }

    public static int[][] getArrayTypeI() {

        int[][] arr = new int[12][10];
        int value = 1;
        for (int i = arr.length - 1; i >= 0; i--) {
            for (int j = arr[i].length - 1; j >= 0; j--) {
                arr[i][j] = value++;
            }
        }
        return arr;
    }

    public static int[][] getArrayTypeK() {

        int[][] arr = new int[12][10];
        int value = 1;

        for (int j = arr[0].length - 1; j >= 0; j--) {
            for (int i = arr.length - 1; i >= 0; i--) {
                arr[i][j] = value++;
            }
        }
        return arr;
    }

    public static int[][] getArrayTypeL() {

        int[][] arr = new int[12][10];
        int value = 1;
        boolean toRight = true;

        for (int i = arr.length - 1; i >= 0; i--) {

            if (toRight) {
                for (int j = 0; j < arr[i].length; j++) {
                    arr[i][j] = value++;
                }
            } else {
                for (int j = arr[i].length - 1; j >= 0; j--) {
                    arr[i][j] = value++;
                }
            }
            toRight = !toRight;
        }
        return arr;
    }

    public static int[][] getArrayTypeM() {

        int[][] arr = new int[12][10];
        int value = 1;
        boolean toRight = false;
        for (int i = 0; i < arr.length; i++) {

            if (toRight) {
                for (int j = 0; j < arr[i].length; j++) {
                    arr[i][j] = value++;
                }
            } else {
                for (int j = arr[i].length - 1; j >= 0; j--) {
                    arr[i][j] = value++;
                }
            }
            toRight = !toRight;

        }
        return arr;
    }

    public static int[][] getArrayTypeN() {

        int[][] arr = new int[12][10];
        int value = 1;
        boolean fromTop = true;

        for (int j = arr[0].length - 1; j >= 0; j--) {

            if (fromTop) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i][j] = value++;
                }
            } else {
                for (int i = arr.length - 1; i >= 0; i--) {
                    arr[i][j] = value++;
                }
            }
            fromTop = !fromTop;
        }
        return arr;
    }

    public static int[][] getArrayTypeO() {

        int[][] arr = new int[12][10];
        int value = 1;
        boolean fromTop = false;

        for (int j = 0; j < arr[0].length; j++) {

            if (fromTop) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i][j] = value++;
                }
            } else {
                for (int i = arr.length - 1; i >= 0; i--) {
                    arr[i][j] = value++;
                }
            }
            fromTop = !fromTop;
        }
        return arr;

    }

    public static int[][] getArrayTypeP() {

        int[][] arr = new int[12][10];
        int value = 1;
        boolean toRight = false;

        for (int i = arr.length - 1; i >= 0; i--) {

            if (toRight) {
                for (int j = 0; j < arr[i].length; j++) {
                    arr[i][j] = value++;
                }
            } else {
                for (int j = arr[i].length - 1; j >= 0; j--) {
                    arr[i][j] = value++;
                }
            }
            toRight = !toRight;
        }
        return arr;
    }

    public static int[][] getArrayTypeR() {

        int[][] arr = new int[12][10];
        boolean toBottom = false;
        int value = 1;

        for (int j = arr[0].length - 1; j >= 0; j--) {

            if (toBottom) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i][j] = value++;
                }
            } else {
                for (int i = arr.length - 1; i >= 0; i--) {
                    arr[i][j] = value++;
                }
            }

            toBottom = !toBottom;
        }

        return arr;
    }

}
