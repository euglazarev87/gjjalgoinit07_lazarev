package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh10N045 {

    public static void main(String[] args) {

        System.out.println("Enter first member:");
        int firstMember = InputReader.nextInt();
        System.out.println("Enter arithmetic difference:");
        int difference = InputReader.nextInt();

        System.out.println("Enter number of needed member:");
        int n = InputReader.nextInt();

        System.out.println("Value of arithmetic progression member " + n + " is " + findValueOfMember(firstMember, difference, n));
        System.out.println("Sum of arithmetic progression members is " + findSumOfProgressionMembers(firstMember, difference, n, 0));

    }

    public static int findValueOfMember(int member, int diff, int n) {

        if (n == 1) {
            return member;
        }
        return findValueOfMember(member + diff, diff, n - 1);
    }

    public static int findSumOfProgressionMembers(int member, int diff, int n, int sum) {

        if (sum == 0) {
            sum = member;
        }

        if (n == 1) {
            return sum;
        }

        sum += findValueOfMember(member, diff, n);
        return findSumOfProgressionMembers(member, diff, n - 1, sum);
    }
}
