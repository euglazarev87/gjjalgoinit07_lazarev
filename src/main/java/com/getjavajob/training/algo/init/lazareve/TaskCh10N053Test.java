package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N053.*;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N053Test {

    public static void main(String[] args) {
        int[] arr = {3, 4, 5, 6, 7, 8};
        assertEquals("TaskCh10N053.getReversedInput", "876543", getReversedInput(arr));
    }
}
