package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh10N053 {

    public static void main(String[] args) {

        System.out.println("Please enter few integers. For exit enter 0");
        int userInput;
        StringBuilder sb = new StringBuilder();

        while (true) {
            userInput = InputReader.nextInt();
            if (userInput == 0) {
                break;
            }
            sb.append(" "+ userInput);
        }

        String[] stringArr = sb.toString().trim().split(" ");
        int[] intArray = new int[stringArr.length];

        for (int i = 0; i < stringArr.length; i++) {
            intArray[i] = Integer.parseInt(stringArr[i]);
        }

        System.out.println("Reversed input is " + getReversedInput(intArray));
    }

    public static String getReversedInput(int[] arr) {
        return getReversedInput(arr, new StringBuilder(), arr.length - 1);
    }

    private static String getReversedInput(int[] arr, StringBuilder sb, int iterator) {

        if (iterator >= 0) {
            sb.append(arr[iterator]);
            getReversedInput(arr, sb, iterator - 1);
            return sb.toString();
        }

        return "";
    }
}
