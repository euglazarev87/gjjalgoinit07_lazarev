package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh04N106.getSeason;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N106Test {

    public static void main(String[] args) {
        assertEquals("TaskCh04N106.getSeason", "Winter", getSeason(12));
        assertEquals("TaskCh04N106.getSeason", "Spring", getSeason(3));
        assertEquals("TaskCh04N106.getSeason", "Summer", getSeason(7));
        assertEquals("TaskCh04N106.getSeason", "Autumn", getSeason(11));
    }
}
