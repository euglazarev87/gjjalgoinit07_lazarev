package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh03N029.*;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh03N029Test {

    public static void main(String[] args) {

        assertEquals("TaskCh03N029.taskA", true, taskA(3, 3));
        assertEquals("TaskCh03N029.taskA", false, taskA(3, 2));
        assertEquals("TaskCh03N029.taskB", true, taskB(2, 300));
        assertEquals("TaskCh03N029.taskB", false, taskB(3, 2));
        assertEquals("TaskCh03N029.taskV", true, taskV(0, 300));
        assertEquals("TaskCh03N029.taskV", false, taskV(3, 2));
        assertEquals("TaskCh03N029.taskG", true, taskG(-1, -300, -9));
        assertEquals("TaskCh03N029.taskG", false, taskG(3, 2, -1));
        assertEquals("TaskCh03N029.taskD", true, taskD(10, 3, 3));
        assertEquals("TaskCh03N029.taskD", false, taskD(1, 2, -1));
        assertEquals("TaskCh03N029.taskE", true, taskE(10, 101, 3));
        assertEquals("TaskCh03N029.taskE", false, taskE(1, 2, -1));
    }
}
