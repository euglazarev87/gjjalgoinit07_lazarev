package com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh09N185.getExpressionTestMessage;
import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh09N185.isCorrectExpression;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N185Test {

    public static void main(String[] args) {
        startTest();
    }

    private static void startTest() {

        assertEquals("TaskCh09N185.isCorrectExpression", true, isCorrectExpression("() + () / ()"));
        assertEquals("TaskCh09N185.isCorrectExpression", false, isCorrectExpression(")() + () / ()"));

        assertEquals("TaskCh09N185.isCorrectExpression", "Expression is correct",
                getExpressionTestMessage("() + () / ()"));
        assertEquals("TaskCh09N185.isCorrectExpression", "Incorrect right bracket at position 1 in expression",
                getExpressionTestMessage(")("));

        assertEquals("TaskCh09N185.isCorrectExpression", "3 incorrect left brackets in expression",
                getExpressionTestMessage("((((())"));

    }
}
