package main.java.com.getjavajob.training.algo.init.lazareve;

public class TaskCh10N050 {

    public static void main(String[] args) {

        int m = 1;
        int n = 3;

        System.out.println("Result of Accerman function for n = " + n + " and m = " + m + " is " + getAccerman(m, n));
    }

    public static int getAccerman(int m, int n) {

        if (m == 0) {
            return n + 1;
        } else if (m > 0 && n == 0) {
            return getAccerman(m - 1, 1);
        } else {
            return getAccerman(m - 1, getAccerman(m, n - 1));
        }
    }
}
