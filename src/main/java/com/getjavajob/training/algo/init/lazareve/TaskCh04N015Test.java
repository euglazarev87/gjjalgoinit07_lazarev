package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh04N015.*;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N015Test {

    public static void main(String[] args) {
        assertEquals("TaskCh04N015.getCurrentAge", 29, getCurrentAge(1985, 6, 2014, 12));
        assertEquals("TaskCh04N015.getCurrentAge", 28, getCurrentAge(1985, 6, 2014, 5));
        assertEquals("TaskCh04N015.getCurrentAge", 29, getCurrentAge(1985, 6, 2014, 6));
    }
}
