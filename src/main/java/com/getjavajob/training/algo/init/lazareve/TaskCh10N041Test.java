package com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N041.getFactorial;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N041Test {

    public static void main(String[] args) {
        assertEquals("TaskCh10N041.getFactorial", 6, getFactorial(3));
        assertEquals("TaskCh10N041.getFactorial", 24, getFactorial(4));
        assertEquals("TaskCh10N041.getFactorial", 120, getFactorial(5));
    }
}
