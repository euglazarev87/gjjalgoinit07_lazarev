package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh12N023.fillArrayA;
import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh12N023.fillArrayB;
import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh12N023.fillArrayV;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N023Test {

    public static void main(String[] args) {

        int[][] arrA = new int[][]{
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}};

        assertEquals("TaskCh12N023.fillArrayA", arrA, fillArrayA(new int[7][7]));

        int[][] arrB = new int[][]{
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}};

        assertEquals("TaskCh12N023.fillArrayB", arrB, fillArrayB(new int[7][7]));

        int[][] arrV = new int[][]{
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}};

        assertEquals("TaskCh12N023.fillArrayV", arrV, fillArrayV(new int[7][7]));
    }

}
