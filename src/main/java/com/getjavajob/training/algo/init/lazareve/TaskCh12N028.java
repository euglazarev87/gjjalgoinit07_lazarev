package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.ArrayPrinter;
import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh12N028 {

    public static void main(String[] args) {

        System.out.println("Please, enter number: ");
        int n = InputReader.nextInt();
        if (n % 2 != 1) {
            System.out.println("Incorrect input");
            return;
        }

        ArrayPrinter.printArray(getFunnelArray(n));
    }

    public static int[][] getFunnelArray(int n) {

        int[][] arr = new int[n][n];

        Direction curDirection = Direction.RIGHT;

        for (int i = 0, j = 0, val = 1; val <= arr.length * arr.length; val++) {

            arr[i][j] = val;

            if (!isNextElementExistAndEmpty(arr, i + curDirection.stepY(), j + curDirection.stepX())) {
                curDirection = curDirection.next();
            }

            i += curDirection.stepY();
            j += curDirection.stepX();
        }
        return arr;
    }

    private static boolean isNextElementExistAndEmpty(int[][] arr, int i, int j) {

        if (i >= 0 && i < arr.length) {
            if (j >= 0 && j < arr[i].length) {
                if (arr[i][j] == 0) {
                    return true;
                }
            }
        }
        return false;
    }

}

enum Direction {

    RIGHT,
    LEFT,
    UP,
    DOWN;

    public Direction next() {

        switch (this) {
            case RIGHT:
                return DOWN;
            case DOWN:
                return LEFT;
            case LEFT:
                return UP;
            case UP:
                return RIGHT;
            default:
                return RIGHT;
        }
    }

    public int stepX() {

        switch (this) {
            case RIGHT:
                return 1;
            case DOWN:
                return 0;
            case LEFT:
                return -1;
            case UP:
                return 0;
            default:
                return 0;
        }
    }

    public int stepY() {

        switch (this) {
            case RIGHT:
                return 0;
            case DOWN:
                return 1;
            case LEFT:
                return 0;
            case UP:
                return -1;
            default:
                return 0;
        }
    }
}
