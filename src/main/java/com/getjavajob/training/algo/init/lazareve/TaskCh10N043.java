package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh10N043 {

    public static void main(String[] args) {
        System.out.println("Please enter a positive integer:");
        int naturalDigit = InputReader.nextInt();
        if (naturalDigit < 1) {
            System.out.println("Invalid input");
            return;
        }
        System.out.println("Sum of digits is " + getSumOfDigits(naturalDigit));
        System.out.println("Count of digits is " + getCountOfDigits(naturalDigit));
    }

    /**
     * Returns sum digits of positive integer
     *
     * @param natural int positive integer
     * @return
     */
    public static int getSumOfDigits(int natural) {

        if (natural <= 0) {
            return 0;
        }

        int res;
        res = natural % 10;
        res += getSumOfDigits(natural / 10);

        return res;
    }

    /**
     * Returns count digits of positive integer
     *
     * @param natural int positive integer
     * @return
     */
    public static int getCountOfDigits(int natural) {

        if (natural <= 0) {
            return 0;
        }

        int res = 1;
        res += getCountOfDigits(natural / 10);
        return res;
    }
}
