package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh09N042 {

    public static void main(String[] args) {

        System.out.println("Please, enter word: ");
        String word = InputReader.next();
        System.out.println("Reversed word is " + "\"" + getReversedWord(word) + "\"");
    }

    public static String getReversedWord(String word) {
        StringBuilder builder = new StringBuilder(word);
        builder.reverse();
        return builder.toString();
    }
}
