package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh09N166 {

    public static void main(String[] args) {

        System.out.println("Please, enter some text: ");
        String text = InputReader.nextLine();
        System.out.println("Result version is - \"" + switchFirstAndLastWords(text) + "\"");
    }

    public static String switchFirstAndLastWords(String text) {

        String[] textArray = text.split(" ");

        if (textArray.length < 2) {
            return text;
        }

        int lastIndex = textArray.length - 1;
        String firstWord = textArray[0];
        String lastWord = textArray[lastIndex];

        textArray[0] = lastWord;
        textArray[lastIndex] = firstWord;

        StringBuilder builder = new StringBuilder();
        for (String str : textArray) {
            builder.append(" " + str);
        }

        return builder.toString().trim();
    }
}
