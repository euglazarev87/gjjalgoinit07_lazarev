package main.java.com.getjavajob.training.algo.init.lazareve;

import java.util.Scanner;

public class TaskCh04N015 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        // get current date
        System.out.print("Please enter current YEAR: ");
        int curYear = scanner.nextInt();

        System.out.println();
        System.out.print("Please enter current MONTH: ");
        int curMonth = scanner.nextInt();
        if (curMonth == 0 || curMonth > 12) {
            System.out.println("Incorrect month");
            return;
        }

        // get brith date
        System.out.print("Please enter YEAR of your birthday: ");
        int birthYear = scanner.nextInt();
        if (birthYear > 2016) {
            System.out.println("You can't live in future :)");
            return;
        }

        System.out.println();
        System.out.print("Please enter MONTH of your birthday: ");
        int birthMonth = scanner.nextInt();
        if (birthMonth == 0 || birthMonth > 12) {
            System.out.println("Incorrect month");
            return;
        }

        System.out.println();
        System.out.println("Your age is " + getCurrentAge(birthYear, birthMonth, curYear, curMonth) + " years");
    }

    public static int getCurrentAge(int birthYear, int birthMonth, int curYear, int curMonth) {
        return ((curYear * 12 + curMonth) - (birthYear * 12 + birthMonth)) / 12;
    }
}
