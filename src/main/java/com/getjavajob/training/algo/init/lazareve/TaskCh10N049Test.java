package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N049.getLastElement;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N049Test {

    public static void main(String[] args) {
        String[] arr = {"el1", "el2", "el2", "el2", "el2", "el2", "el2", "el2", "el2", "el2", "el2", "el900",};
        assertEquals("TaskCh10N049.getLastElement", "el900", getLastElement(arr));
    }
}
