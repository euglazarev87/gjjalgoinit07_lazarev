package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.ArrayPrinter;
import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh12N063 {

    public static void main(String[] args) {

        int[][] arr = new int[11][4];

        for (int i = 0; i < arr.length; i++) {
            System.out.println("Enter number of students for " + (i + 1) + " parallel");
            for (int j = 0; j < arr[i].length; j++) {
                System.out.println("for " + (j + 1) + " class");
                arr[i][j] = InputReader.nextInt();
            }
        }

        /*// random array generator
        for (int i = 0; i < arr.length; i++){
            for (int j = 0; j < arr[i].length; j++){
                arr[i][j]=  (int)((Math.random() * 20) + 25);
            }
        }*/

        ArrayPrinter.printArray(arr);
        System.out.println();

        ArrayPrinter.printArray(getAverageNumber(arr));

    }

    public static int[] getAverageNumber(int[][] arr) {

        int[] result = new int[arr.length];
        int i = 0;
        int sum;
        for (int[] parallel : arr) {
            sum = 0;
            for (int studentCount : parallel) {
                sum += studentCount;
            }

            result[i] = sum / parallel.length;
            i++;
        }
        return result;
    }

}
