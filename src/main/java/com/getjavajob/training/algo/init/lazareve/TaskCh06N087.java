package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh06N087 {

    public static void main(String[] args) {
        Game game = new Game();
        game.startGame();
    }
}

class Game {

    public String firstTeamName;
    public String secondTeamName;
    private int firstTeamCount;
    private int secondTeamCount;

    private boolean firstTeamTurn;

    public void startGame() {

        System.out.println("Please enter name of the first team: ");
        firstTeamName = InputReader.next();

        System.out.println("Please enter name of the second team: ");
        secondTeamName = InputReader.next();

        System.out.println("Enter 0 to end game and 1,2 or 3 to add point for team.");

        firstTeamTurn = true;
        while (nextTurn()) {
        }
    }

    public boolean nextTurn() {

        printCurrentAccount();

        if (firstTeamTurn) {
            System.out.println("First team turn: ");
        } else {
            System.out.println("Second team turn: ");
        }

        int points = InputReader.nextInt();
        if (points == 0) {
            endGame();
            return false;
        }

        if (!(points == 1 || points == 2 || points == 3)) {
            System.out.println("Invalid input");
            return true;
        }

        if (firstTeamTurn) {
            firstTeamCount += points;
        } else {
            secondTeamCount += points;
        }
        firstTeamTurn = !firstTeamTurn;
        return true;
    }

    public void endGame() {

        printCurrentAccount();
        if (firstTeamCount == secondTeamCount) {
            System.out.println("Draw!");
            return;
        }

        String winnersName = firstTeamCount > secondTeamCount ? firstTeamName : secondTeamName;
        System.out.println("Game ended. Winner is " + winnersName + " team");
    }

    public void printCurrentAccount() {
        System.out.println("Current account is: ");
        System.out.println(firstTeamName + " " + firstTeamCount + " : " + secondTeamCount + " " + secondTeamName);
    }
}
