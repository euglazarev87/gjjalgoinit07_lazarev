package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N055Test {

    public static void main(String[] args) {
        TaskCh10N055 task = new TaskCh10N055();
        assertEquals("TaskCh10N055.convertIntoNumberSystem", "1100100", task.convertIntoNumberSystem(100, 2));
        assertEquals("TaskCh10N055.convertIntoNumberSystem", "10201", task.convertIntoNumberSystem(100, 3));
        assertEquals("TaskCh10N055.convertIntoNumberSystem", "1210", task.convertIntoNumberSystem(100, 4));
        assertEquals("TaskCh10N055.convertIntoNumberSystem", "400", task.convertIntoNumberSystem(100, 5));
        assertEquals("TaskCh10N055.convertIntoNumberSystem", "244", task.convertIntoNumberSystem(100, 6));
        assertEquals("TaskCh10N055.convertIntoNumberSystem", "202", task.convertIntoNumberSystem(100, 7));
        assertEquals("TaskCh10N055.convertIntoNumberSystem", "144", task.convertIntoNumberSystem(100, 8));
        assertEquals("TaskCh10N055.convertIntoNumberSystem", "121", task.convertIntoNumberSystem(100, 9));
        assertEquals("TaskCh10N055.convertIntoNumberSystem", "100", task.convertIntoNumberSystem(100, 10));
        assertEquals("TaskCh10N055.convertIntoNumberSystem", "6A", task.convertIntoNumberSystem(100, 15));
        assertEquals("TaskCh10N055.convertIntoNumberSystem", "64", task.convertIntoNumberSystem(100, 16));
    }
}
