package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N046.findSumOfProgressionMembers;
import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N046.findValueOfMember;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N046Test {

    public static void main(String[] args) {
        assertEquals("TaskCh10N046.findSumOfProgressionMembers", 327670, findSumOfProgressionMembers(10, 2, 15, 0));
        assertEquals("TaskCh10N046.findValueOfMember", 163840, findValueOfMember(10, 2, 15));
    }
}
