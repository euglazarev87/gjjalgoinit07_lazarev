package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh09N017 {

    public static void main(String[] args) {

        System.out.println("Enter the word: ");
        String word = InputReader.next();

        System.out.println("Is the first and last char in this word same? " + isFirstAndLastCharSame(word));
    }

    public static boolean isFirstAndLastCharSame(String word) {

        if (word.length() == 0 || word.length() == 1) {
            return false;
        }

        return word.charAt(0) == word.charAt(word.length() - 1);
    }
}
