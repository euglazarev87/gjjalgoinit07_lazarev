package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh04N067.isWWeekend;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N067Test {

    public static void main(String[] args) {
        assertEquals("TaskCh04N067.isWWeekend", false, isWWeekend(1));
        assertEquals("TaskCh04N067.isWWeekend", false, isWWeekend(2));
        assertEquals("TaskCh04N067.isWWeekend", false, isWWeekend(3));
        assertEquals("TaskCh04N067.isWWeekend", false, isWWeekend(4));
        assertEquals("TaskCh04N067.isWWeekend", false, isWWeekend(5));
        assertEquals("TaskCh04N067.isWWeekend", true, isWWeekend(6));
        assertEquals("TaskCh04N067.isWWeekend", true, isWWeekend(7));
    }
}
