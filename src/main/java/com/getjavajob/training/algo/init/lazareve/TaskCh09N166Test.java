package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh09N166.switchFirstAndLastWords;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N166Test {

    public static void main(String[] args) {
        startTest();
    }

    private static void startTest() {
        assertEquals("TaskCh09N166.switchFirstAndLastWords", "awsome! is Java", switchFirstAndLastWords("Java is awsome!"));
        assertEquals("TaskCh09N166.switchFirstAndLastWords", "Trololo", switchFirstAndLastWords("Trololo"));
        assertEquals("TaskCh09N166.switchFirstAndLastWords", "__! ", switchFirstAndLastWords("__! "));
    }
}
