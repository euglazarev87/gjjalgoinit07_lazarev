package main.java.com.getjavajob.training.algo.init.lazareve;

import java.util.Scanner;

public class TaskCh02N039 {

    public static void main(String[] args) {

        System.out.println("Please enter 3 int - hour, minute, second");
        System.out.print("Hour (from 1 to 23) - ");
        Scanner sc = new Scanner(System.in);
        int h = sc.nextInt();

        System.out.print("Minute (from 1 to 59) - ");
        int m = sc.nextInt();

        System.out.print("Second (from 1 to 59) - ");
        int s = sc.nextInt();

        if ((h < 1 || h > 23) || (m < 1 || h > 59) || (s < 1 || h > 59)) {
            System.out.println("Invalid input");
            return;
        }
        double degreeOfAngle = getDegressClockwise(h, m, s);
        System.out.println("The angle clockwise from start of the day to the specified point in time is " + degreeOfAngle);
    }

    /**
     * Returns angle size for specified time
     * @param h - hours
     * @param m - minuts
     * @param s - seconds
     * @return angle size
     */
    public static double getDegressClockwise(int h, int m, int s) {

        if (h > 12) {
            h -= 12;
        }
        int secondsInHour = 60 * 60;
        double secondsOnClockFace = secondsInHour * 12;
        double degreePerSecond = 360 / secondsOnClockFace;
        int secondsInMomentOfTime = (h * secondsInHour) + (m * 60) + s;

        return secondsInMomentOfTime * degreePerSecond;
    }
}
