package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh04N036 {

    public static void main(String[] args) {

        System.out.println("Please enter number");
        int number = InputReader.nextInt();

        if (number < 1 || number > 60) {
            System.out.println("Invalid input");
            return;
        }

        String color = currentTrafficLightIsGreen(number) ? "green" : "red";
        System.out.println("Current traffic light is " + color);

    }

    public static boolean currentTrafficLightIsGreen(int minute) {

        boolean isGreen = true;
        for (int i = 1, j = 0; i <= minute; i++, j++) {

            if (isGreen) {
                if (j == 3) {
                    isGreen = false;
                    j = 0;
                }
            } else {
                if (j == 2) {
                    isGreen = true;
                    j = 0;
                }
            }

        }
        return isGreen;
    }
}
