package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh05N064.getAveragePopulationDensity;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N064Test {

    public static void main(String[] args) {

        int[][] arr = {
                {20, 500},
                {30, 900},
                {20, 500},
                {30, 900},
                {20, 500},
                {30, 900},
                {20, 500},
                {30, 900},
                {20, 500},
                {30, 900},
                {20, 500},
                {30, 900}
        };

        assertEquals("TaskCh05N064.getAveragePopulationDensity", 28, getAveragePopulationDensity(arr));
    }
}
