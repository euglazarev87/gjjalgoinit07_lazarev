package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh09N017.isFirstAndLastCharSame;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N017Test {

    public static void main(String[] args) {
        assertEquals("TaskCh09N017.isFirstAndLastCharSame", false, isFirstAndLastCharSame("hulio"));
        assertEquals("TaskCh09N017.isFirstAndLastCharSame", true, isFirstAndLastCharSame("hulioh"));
        assertEquals("TaskCh09N017.isFirstAndLastCharSame", true, isFirstAndLastCharSame("aa"));
        assertEquals("TaskCh09N017.isFirstAndLastCharSame", false, isFirstAndLastCharSame("h"));
        assertEquals("TaskCh09N017.isFirstAndLastCharSame", false, isFirstAndLastCharSame(""));
    }
}

