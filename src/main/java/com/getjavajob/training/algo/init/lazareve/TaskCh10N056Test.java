package com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N056.isPrime;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N056Test {

    public static void main(String[] args) {
        assertEquals("TaskCh10N056.isPrime", false, isPrime(105));
        assertEquals("TaskCh10N056.isPrime", true, isPrime(107));
    }
}
