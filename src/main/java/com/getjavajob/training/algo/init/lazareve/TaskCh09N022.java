package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh09N022 {

    public static void main(String[] args) {

        System.out.println("Enter word with even count of letters: ");
        String word = InputReader.next();
        if (word.isEmpty() || word.length() % 2 != 0) {
            System.out.println("Invalid input");
            return;
        }

        System.out.println("First half of the word is " + getFirstHalfOfTheWord(word));
    }

    public static String getFirstHalfOfTheWord(String word) {
        return word.substring(0, word.length() / 2);
    }
}
