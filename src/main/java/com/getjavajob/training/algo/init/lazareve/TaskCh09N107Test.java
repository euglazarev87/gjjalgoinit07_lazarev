package com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh09N107.mixFirstAAndLastOLetters;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N107Test {

    public static void main(String[] args) {
        startTest();
    }

    private static void startTest() {

        assertEquals("TaskCh09N107.mixFirstAAndLastOLetters", "oloalaha", mixFirstAAndLastOLetters("aloaloha"));
        assertEquals("TaskCh09N107.mixFirstAAndLastOLetters", "hruhru", mixFirstAAndLastOLetters("hruhru"));
        assertEquals("TaskCh09N107.mixFirstAAndLastOLetters", "", mixFirstAAndLastOLetters(""));
        assertEquals("TaskCh09N107.mixFirstAAndLastOLetters", "alala", mixFirstAAndLastOLetters("alala"));
    }
}
