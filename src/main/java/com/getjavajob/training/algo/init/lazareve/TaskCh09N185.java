package main.java.com.getjavajob.training.algo.init.lazareve;

public class TaskCh09N185 {

    public static void main(String[] args) {

        String source = "(a + b) / ((c / 19) / 20) * (90 * ((a * 2) + (b * 2) / 7))";
        System.out.println(isCorrectExpression(source) ? "Yes" : "No");
        System.out.println(getExpressionTestMessage(source));
    }

    public static boolean isCorrectExpression(String exp) {

        String brackets = exp.replaceAll("[^()]", "");
        int balance = 0;
        for (int i = 0; i < brackets.length(); i++) {

            switch (brackets.charAt(i)) {
                case '(':
                    balance += 1;
                    break;
                case ')':
                    balance -= 1;
                    break;
            }
        }

        return balance == 0;
    }

    public static String getExpressionTestMessage(String exp) {

        int balance = 0;
        for (int i = 0; i < exp.length(); i++) {

            switch (exp.charAt(i)) {
                case '(':
                    balance += 1;
                    break;
                case ')':
                    balance -= 1;
                    break;
                default:
                    continue;
            }

            if (balance < 0) {
                return "Incorrect right bracket at position " + (i + 1) + " in expression";
            }
        }

        if (balance > 0) {
            return balance + " incorrect left brackets in expression";
        }

        return "Expression is correct";
    }

}
