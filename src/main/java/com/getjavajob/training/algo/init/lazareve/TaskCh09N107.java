package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh09N107 {

    public static void main(String[] args) {

        System.out.println("Please, enter some word: ");
        String word = InputReader.next();
        System.out.println("Result of word mix is " + mixFirstAAndLastOLetters(word));
    }

    public static String mixFirstAAndLastOLetters(String word) {

        StringBuilder builder = new StringBuilder(word.toLowerCase());

        String a = "a";
        String o = "o";

        int aFirstIndex = builder.indexOf(a);
        int oLastIndex = builder.lastIndexOf(o);
        if (aFirstIndex != -1 && oLastIndex != -1) {
            builder.deleteCharAt(aFirstIndex);
            builder.insert(aFirstIndex, o);

            builder.deleteCharAt(oLastIndex);
            builder.insert(oLastIndex, a);
        }

        return builder.toString();
    }
}
