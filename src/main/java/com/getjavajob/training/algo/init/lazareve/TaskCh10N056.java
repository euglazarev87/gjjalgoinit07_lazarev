package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh10N056 {

    public static void main(String[] args) {

        System.out.println("Please enter positive integer: ");
        int number = InputReader.nextInt();
        if (number < 1 && 10000 > number) {
            System.out.println("Invalid input");
            return;
        }

        System.out.println("Is number " + number + " prime? " + isPrime(number));
    }

    public static boolean isPrime(int n) {
        return isPrime(n, 2);
    }

    private static boolean isPrime(int n, int d) {

        if (n == 1) {
            return false;
        }

        if (d <= Math.sqrt(n)) {
            if (n % d == 0) {
                return false;
            } else {
                return isPrime(n, d + 1);
            }
        }
        return true;
    }
}
