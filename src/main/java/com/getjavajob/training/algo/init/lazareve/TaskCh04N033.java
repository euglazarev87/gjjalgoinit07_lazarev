package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh04N033 {

    public static void main(String[] args) {

        System.out.println("Please enter positive integer");
        int naturalNumber = InputReader.nextInt();

        System.out.println("This ends with even number - " + Boolean.toString(endsWithEvenNumber(naturalNumber)));
        System.out.println("This ends with odd number - " + Boolean.toString(endsWithOddNumber(naturalNumber)));
    }

    public static boolean endsWithEvenNumber(int number) {
        return number % 2 == 0;
    }

    public static boolean endsWithOddNumber(int number) {
        return number % 2 != 0;
    }
}
