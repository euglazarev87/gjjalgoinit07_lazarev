package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh04N115 {

    public static void main(String[] args) {

        System.out.print("Please enter the year: ");
        int year = InputReader.nextInt();
        if (year < 1) {
            System.out.println("Invalid year");
            return;
        }

        System.out.println("Result is " + getFullYearName(year, true));

    }

    private static int getBeginingYearOfCycle(int year, boolean useBegin1984) {

        int i = useBegin1984 ? 1984 : 1;
        int beginCycleYear = 0;
        for (; i <= year; i += 60) {
            beginCycleYear = i;
        }

        return beginCycleYear;
    }

    public static String getFullYearName(int year, boolean useBegin1984) {

        int beginOfCycle = getBeginingYearOfCycle(year, useBegin1984);
        int indexOfYear = 0;
        int indexOfColor = 0;
        for (int i = beginOfCycle; i <= year; i++) {

            indexOfYear++;
            indexOfColor++;

            if (indexOfYear > 12) {
                indexOfYear = 1;
            }

            if (indexOfColor > 10) {
                indexOfColor = 1;
            }
        }

        return getNameOfTheYear(indexOfYear) + ", " + getColorOfYear(indexOfColor);
    }

    private static String getNameOfTheYear(int numberOfYear) {

        String name;
        switch (numberOfYear) {
            case 1:
                name = "Rat";
                break;
            case 2:
                name = "Cow";
                break;
            case 3:
                name = "Tiger";
                break;
            case 4:
                name = "Rabbit";
                break;
            case 5:
                name = "Dragon";
                break;
            case 6:
                name = "Snake";
                break;
            case 7:
                name = "Horse";
                break;
            case 8:
                name = "Sheep";
                break;
            case 9:
                name = "Monkey";
                break;
            case 10:
                name = "Chicken";
                break;
            case 11:
                name = "Dog";
                break;
            case 12:
                name = "Pork";
                break;
            default:
                name = "Unnamed";
        }
        return name;
    }

    private static String getColorOfYear(int year) {

        String color;
        switch (year) {
            case 1:
            case 2:
                color = "Green";
                break;
            case 3:
            case 4:
                color = "Red";
                break;
            case 5:
            case 6:
                color = "Yellow";
                break;
            case 7:
            case 8:
                color = "White";
                break;
            case 9:
            case 10:
                color = "Black";
                break;
            default:
                color = "No name";
        }

        return color;
    }
}
