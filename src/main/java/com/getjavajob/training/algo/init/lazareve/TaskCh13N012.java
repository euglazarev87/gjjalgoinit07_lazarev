package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

import java.util.ArrayList;

public class TaskCh13N012 {

    public static void main(String[] args) {

        Database db = Database.getInstance();
        db.add(new Employee("Lazarev", "Evgenii", "Georgievich", "Moldibaeva 38a", 2014, 9));
        db.add(new Employee("Lazareva", "Ekaterina", "Alekseevna", "Moldibaeva 38a", 2000, 5));
        db.add(new Employee("Lazarev", "Makism", "Evgenevich", "Moldibaeva 38a", 2015, 3));
        db.add(new Employee("Ahmetov", "Damir", "Gogolja 3", 2009, 1));
        db.add(new Employee("Krasnova", "Ekaterina", "Gogolja 16 38a", 2014, 12));
        db.add(new Employee("Kachkimbaev", "Fedor", "Gogolja 38", 2013, 8));
        db.add(new Employee("Bekujin", "Aleksei", "Rafaelievich", "Moldibaeva 38a", 2013, 9));
        db.add(new Employee("Romanov", "Stepan", "Moldibaeva 38a", 2010, 7));
        db.add(new Employee("Usubaliev", "Mihail", "Moldibaeva 38a", 2009, 9));
        db.add(new Employee("Kirillov", "Stanislav", "Moldibaeva 38a", 2008, 11));
        db.add(new Employee("Kambarov", "Dmitrii", "Moldibaeva 38a", 2014, 12));
        db.add(new Employee("Shumaher", "Julia", "Moldibaeva 38a", 2014, 9));
        db.add(new Employee("Kozlov", "Evgenii", "Moldibaeva 38a", 2005, 3));
        db.add(new Employee("Gavrish", "Svetlana", "Moldibaeva 38a", 2013, 9));
        db.add(new Employee("Rodin", "Denis", "Moldibaeva 38a", 2016, 1));
        db.add(new Employee("Shinkarev", "Andrei", "Vladimirovich", "Moldibaeva 38a", 2016, 2));
        db.add(new Employee("Metrofanov", "Denis", "Vitlevich", "Kvadrat", 2016, 1));
        db.add(new Employee("Toktaev", "Azamat", "Toktogulovich", "Gogolja 112", 2015, 2));
        db.add(new Employee("Zadkov", "Igor", "Aleksandrovich", "Sidikova 126", 2015, 9));
        db.add(new Employee("Emeljanenko", "Fedor", "Starii Oskol", 2014, 12));

        System.out.println("Please, enter search string");
        String substr = InputReader.next();

        System.out.println("Result of search by substring \"" + substr + "\":");
        for (Employee e : db.findBySubstring(substr)) {
            System.out.println(e.getFullName());
        }

        System.out.println();
        System.out.println("Please, enter minimum years of experience:");
        int exp = InputReader.nextInt();
        System.out.println("Workers with expierence " + exp + " years are:");
        for (Employee e : db.findEmployeesWithExperience(exp, 2016, 3)) {
            System.out.println(e.getFullName());
        }
    }
}

class Employee {

    private String surname;
    private String name;
    private String patronymic;
    private String address;
    private int beginYear;
    private int beginMonth;

    public Employee(String surname, String name, String address, int beginYear, int beginMonth) {
        this(surname, name, "", address, beginYear, beginMonth);
    }

    public Employee(String surname, String name, String patronymic, String address, int beginYear, int beginMonth) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.address = address;
        this.beginYear = beginYear;
        this.beginMonth = beginMonth;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getBeginYear() {
        return beginYear;
    }

    public void setBeginYear(int beginYear) {
        this.beginYear = beginYear;
    }

    public int getBeginMonth() {
        return beginMonth;
    }

    public void setBeginMonth(int beginMonth) {
        this.beginMonth = beginMonth;
    }

    public double getLengthOfWork(int year, int month) {
        return (double) ((year * 12 + month) - (this.beginYear * 12 + this.beginMonth)) / 12;
    }

    public String getFullName() {
        return getSurname() + " " + getName() + " " + getPatronymic();
    }
}

class Database {

    private static Database instance;
    private ArrayList<Employee> list;

    private Database() {
    }

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
            instance.list = new ArrayList<>();
        }
        return instance;
    }

    public void add(Employee employee) {
        list.add(employee);
    }

    public ArrayList<Employee> findBySubstring(String search) {

        ArrayList<Employee> result = new ArrayList<>();
        for (Employee emp : this.list) {

            if (emp.getSurname().toLowerCase().indexOf(search.toLowerCase()) != -1) {
                result.add(emp);
                continue;
            }

            if (emp.getName().toLowerCase().indexOf(search.toLowerCase()) != -1) {
                result.add(emp);
                continue;
            }

            if (emp.getPatronymic().toLowerCase().indexOf(search.toLowerCase()) != -1) {
                result.add(emp);
            }

        }

        return result;
    }

    public ArrayList<Employee> findEmployeesWithExperience(int years, int curYear, int curMonth) {

        ArrayList<Employee> result = new ArrayList<>();
        for (Employee e : this.list) {
            if (e.getLengthOfWork(curYear, curMonth) >= years) {
                result.add(e);
            }
        }
        return result;
    }

}