package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.ArrayPrinter;
import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh05N010 {

    public static void main(String[] args) {

        System.out.print("Please enter current rate: ");
        int rate = InputReader.nextInt();

        int[] range = new int[20];
        for (int i = 0; i < range.length; i++) {
            range[i] = i + 1;
        }

        ArrayPrinter.printArray(getRatesForRange(range, rate));
    }

    public static String[] getRatesForRange(int[] range, int rate) {
        String[] result = new String[range.length];
        for (int i = 0; i < range.length; i++) {
            result[i] = getRate(range[i], rate);
        }
        return result;
    }

    private static String getRate(int sum, int rate) {
        return sum + " USD is " + sum * rate + " RUB";
    }

}
