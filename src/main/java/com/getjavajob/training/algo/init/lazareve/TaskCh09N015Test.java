package main.java.com.getjavajob.training.algo.init.lazareve;


import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh09N015.getCharWithNumberInWord;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N015Test {

    public static void main(String[] args) {
        assertEquals("TaskCh09N015.getCharWithNumberInWord", 't', getCharWithNumberInWord("city", 3));
        assertEquals("TaskCh09N015.getCharWithNumberInWord", 's', getCharWithNumberInWord("Moscow", 3));
    }

}
