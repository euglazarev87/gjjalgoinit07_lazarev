package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh09N015 {

    public static void main(String[] args) {

        System.out.println("Enter a word");
        String word = InputReader.next();

        System.out.println("Enter a number of char witch you will see: ");
        int k = InputReader.nextInt();

        if (k > word.length()) {
            System.out.println("Incorrect enter");
            return;
        }

        System.out.println("Char in entered word with number " + k + " is " + getCharWithNumberInWord(word, k));
    }

    public static char getCharWithNumberInWord(String word, int number) {
        return word.charAt(number - 1);
    }
}
