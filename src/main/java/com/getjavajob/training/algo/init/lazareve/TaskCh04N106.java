package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh04N106 {

    public static void main(String[] args) {

        System.out.println("Please enter number of month (1 <= number <= 12)");
        int month = InputReader.nextInt();

        System.out.println("Entered month is " + getSeason(month));
    }

    public static String getSeason(int monthNumber) {

        String season;
        switch (monthNumber) {
            case 12:
            case 1:
            case 2:
                season = "Winter";
                break;
            case 3:
            case 4:
            case 5:
                season = "Spring";
                break;
            case 6:
            case 7:
            case 8:
                season = "Summer";
                break;
            case 9:
            case 10:
            case 11:
                season = "Autumn";
                break;
            default:
                season = "unknown season";
        }
        return season;
    }
}