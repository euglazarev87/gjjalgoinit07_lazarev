package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh12N063.*;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N063Test {

    public static void main(String[] args) {

        int[][] source = {
                {34, 41, 30, 30},
                {41, 32, 37, 26},
                {28, 43, 41, 40},
                {44, 29, 40, 37},
                {42, 39, 39, 25},
                {35, 37, 35, 42},
                {34, 44, 35, 42},
                {39, 35, 32, 32},
                {26, 25, 28, 40},
                {39, 33, 32, 44},
                {40, 25, 35, 38}};

        int[] result = {33, 34, 38, 37, 36, 37, 38, 34, 29, 37, 34};

        assertEquals("TaskCh12N063.getAverageNumber", result, getAverageNumber(source));
    }
}
