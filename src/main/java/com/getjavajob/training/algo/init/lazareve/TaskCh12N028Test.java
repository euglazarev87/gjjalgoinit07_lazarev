package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh12N028.*;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N028Test {

    public static void main(String[] args) {

        int[][] sample = {
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}
        };

        assertEquals("TaskCh12N028.getFunnelArray", sample, getFunnelArray(5));
    }
}
