package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh02N043 {

    public static void main(String[] args) {

        System.out.println("Please enter the two integers:");
        int a = InputReader.nextInt();
        int b = InputReader.nextInt();

        System.out.println("Result of transformation is " + getResult(a, b));
    }

    /**
     * Returs 1 if a divided by b and b divided by a, and any number else.
     * @param a
     * @param b
     * @return
     */
    public static int getResult(int a, int b) {
        return (a % b) * (b % a) + 1;
    }
}
