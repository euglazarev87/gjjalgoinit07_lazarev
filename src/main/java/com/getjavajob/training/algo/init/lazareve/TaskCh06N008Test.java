package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N008Test {

    public static void main(String[] args) {
        assertEquals("TaskCh06N008.getNotExceedNNumbers",
                new double[]{1.0, 4.0, 9.0, 16.0, 25.0, 36.0, 49.0, 64.0},
                TaskCh06N008.getNotExceedNNumbers(67));
    }
}
