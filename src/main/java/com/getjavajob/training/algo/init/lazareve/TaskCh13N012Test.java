package main.java.com.getjavajob.training.algo.init.lazareve;

import java.util.ArrayList;

public class TaskCh13N012Test {

    public static void main(String[] args) {

        Database db = Database.getInstance();
        Employee worker1 = new Employee("Lazarev", "Evgenii", "Georgievich", "Moldibaeva 38a", 2014, 9);
        Employee worker2 = new Employee("Lazareva", "Ekaterina", "Alekseevna", "Moldibaeva 38a", 2000, 5);
        Employee worker3 = new Employee("Lazarev", "Makism", "Evgenevich", "Moldibaeva 38a", 2015, 3);
        Employee worker4 = new Employee("Ahmetov", "Damir", "Gogolja 3", 2009, 1);
        Employee worker5 = new Employee("Krasnova", "Ekaterina", "Gogolja 16 38a", 2014, 12);
        Employee worker6 = new Employee("Kachkimbaev", "Fedor", "Gogolja 38", 2013, 8);
        Employee worker7 = new Employee("Bekujin", "Aleksei", "Rafaelievich", "Moldibaeva 38a", 2013, 9);

        db.add(worker1);
        db.add(worker2);
        db.add(worker3);
        db.add(worker4);
        db.add(worker5);
        db.add(worker6);
        db.add(worker7);

        ArrayList<Employee> sample = new ArrayList<>();
        sample.add(worker1);
        sample.add(worker2);
        sample.add(worker3);

        assertEquals("TaskCh12N012.Database.findBySubstring", sample, Database.getInstance().findBySubstring("Lazar"));

        ArrayList<Employee> sampleE = new ArrayList<>();
        sampleE.add(worker2);
        assertEquals("TaskCh12N012.Database.findEmployeesWithExperience", sampleE,
                Database.getInstance().findEmployeesWithExperience(10, 2016, 1));
    }

    /**
     * Implement test method here because cant import Employee in Assert class
     */
    public static void assertEquals(String methodName, ArrayList<Employee> expected, ArrayList<Employee> actual) {
        if (expected.equals(actual)) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + expected.toString() + ", actual " + actual.toString());
        }
    }
}
