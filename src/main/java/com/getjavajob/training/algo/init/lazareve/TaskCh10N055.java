package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;
import java.util.HashMap;
import java.util.Map;

public class TaskCh10N055 {

    private static Map<String, String> dictionary;

    public TaskCh10N055() {
        // initialization
        dictionary = new HashMap<>();
        dictionary.put("0", "0");
        dictionary.put("1", "1");
        dictionary.put("2", "2");
        dictionary.put("3", "3");
        dictionary.put("4", "4");
        dictionary.put("5", "5");
        dictionary.put("6", "6");
        dictionary.put("7", "6");
        dictionary.put("8", "8");
        dictionary.put("9", "9");
        dictionary.put("10", "A");
        dictionary.put("11", "B");
        dictionary.put("12", "C");
        dictionary.put("13", "D");
        dictionary.put("14", "E");
        dictionary.put("15", "F");
    }

    public static void main(String[] args) {

        System.out.println("Please enter positive number:");
        int integer = InputReader.nextInt();
        if (integer < 1) {
            System.out.println("Invalid input");
            return;
        }

        System.out.println("Enter the base number system: (2 <= N <= 16)");
        int baseSystem = InputReader.nextInt();
        if (baseSystem < 2 || baseSystem > 16) {
            System.out.println("Invalid input");
            return;
        }

        TaskCh10N055 task = new TaskCh10N055();
        System.out.println("Result of converting is " + task.convertIntoNumberSystem(integer, baseSystem));
    }

    public String convertIntoNumberSystem(int integer, int system) {
        return convertIntoNumberSystem(integer, system, new StringBuilder());
    }

    private String convertIntoNumberSystem(int integer, int system, StringBuilder sb) {

        if (integer == 0) {
            return "";
        }

        convertIntoNumberSystem(integer / system, system, sb);
        sb.append(dictionary.get(Integer.toString(integer % system)));
        return sb.toString();
    }
}
