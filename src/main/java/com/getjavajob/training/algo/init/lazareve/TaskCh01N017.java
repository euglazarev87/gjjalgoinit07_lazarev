package main.java.com.getjavajob.training.algo.init.lazareve;

import static java.lang.Math.*;

public class TaskCh01N017 {

    public static void main(String[] args) {

        double a = 2;
        double b = 5;
        double c = 7;
        double x = 10;

        System.out.println("Result of 'O' option = " + getOptionOResult(x));
        System.out.println("Result of 'P' option = " + getOptionPResult(a, b, c, x));
        System.out.println("Result of 'R' option = " + getOptionRResult(x));
        System.out.println("Result of 'S' option = " + getOptionSResult(x));

    }

    /*
    *  returns result of "o" option
    * */
    public static double getOptionOResult(double x) {
        return sqrt(1 - (pow(sin(x), 2)));
    }

    /*
    *  returns result of "p" option
    * */
    public static double getOptionPResult(double a, double b, double c, double x) {
        return 1 / sqrt((a * pow(x, 2)) + (b * x) + c);
    }

    /*
    *  returns result of "r" option
    * */
    public static double getOptionRResult(double x) {
        return (sqrt(x + 1) + sqrt(x - 1)) / (2 * sqrt(x));
    }

    /*
    *  returns result of "s" option
    * */
    public static double getOptionSResult(double x) {
        return abs(x) + abs(x + 1);
    }

}