package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N042.getPowerOf;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N042Test {

    public static void main(String[] args) {
        assertEquals("TaskCh10N042.getPowerOf", 100, getPowerOf(10, 2));
        assertEquals("TaskCh10N042.getPowerOf", 1024, getPowerOf(2, 10));
    }

}
