package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh04N115.getFullYearName;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N115Test {

    public static void main(String[] args) {
        assertEquals("TaskCh04N115.getFullYearName", "Dragon, Yellow", getFullYearName(1988, true));
        assertEquals("TaskCh04N115.getFullYearName", "Rat, Red", getFullYearName(13, false));
        assertEquals("TaskCh04N115.getFullYearName", "Dragon, Black", getFullYearName(2012, true));
        assertEquals("TaskCh04N115.getFullYearName", "Rabbit, Red", getFullYearName(1987, true));
    }
}
