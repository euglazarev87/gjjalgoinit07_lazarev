package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N050.getAccerman;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N050Test {

    public static void main(String[] args) {
        assertEquals("TaskCh10N050.getAccerman", 5, getAccerman(1, 3));
    }
}
