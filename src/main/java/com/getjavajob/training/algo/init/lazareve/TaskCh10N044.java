package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh10N044 {

    public static void main(String[] args) {

        System.out.println("Enter positive integer");
        int number = InputReader.nextInt();

        System.out.println("Digital root of " + number + " is " + getDigitalRoot(number));
    }

    private static int getIntermediateDigitalRoot(int number) {

        if (number == 0) {
            return 0;
        }

        return number % 10 + getIntermediateDigitalRoot(number / 10);
    }

    public static int getDigitalRoot(int number) {

        int result = getIntermediateDigitalRoot(number);
        if (result / 10 == 0) {
            return result;
        }

        return getDigitalRoot(result);
    }
}
