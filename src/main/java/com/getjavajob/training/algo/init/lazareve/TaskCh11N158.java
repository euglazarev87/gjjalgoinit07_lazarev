package main.java.com.getjavajob.training.algo.init.lazareve;

import java.util.Arrays;

public class TaskCh11N158 {

    public static void main(String[] args) {
        int[] source = {0, 1, 2, 3, 4, 4, 5, 6, 7, 8, 7, 7, 9, 10};
        System.out.println(Arrays.toString(removeDuplicates(source)));
    }

    public static int[] removeDuplicates(int[] source) {

        int[] result = new int[source.length];
        int lastIndex = 0;

        for (int i = 0; i < source.length; i++) {

            boolean duplicate = false;
            for (int j = 0; j < result.length; j++) {
                if (source[i] == result[j]) {
                    duplicate = true;
                }
            }

            if (!duplicate) {
                result[lastIndex] = source[i];
                lastIndex++;
            }

        }

        return result;
    }

}
