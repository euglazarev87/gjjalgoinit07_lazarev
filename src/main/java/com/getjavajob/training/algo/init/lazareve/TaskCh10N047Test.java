package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N047.getElementValue;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N047Test {

    public static void main(String[] args) {
        assertEquals("TaskCh10N047.getElementValue", 21, getElementValue(8));
        assertEquals("TaskCh10N047.getElementValue", 34, getElementValue(9));
    }
}
