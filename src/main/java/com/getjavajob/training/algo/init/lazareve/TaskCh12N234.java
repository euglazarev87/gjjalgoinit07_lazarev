package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.ArrayPrinter;
import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh12N234 {

    public static void main(String[] args) {

        int[][] source = {
                {1, 2, 3, 4, 5, 6, 7, 8, 9},
                {2, 2, 3, 4, 5, 6, 7, 8, 9},
                {3, 2, 3, 4, 5, 6, 7, 8, 9},
                {4, 2, 3, 4, 5, 6, 7, 8, 9},
                {5, 2, 3, 4, 5, 6, 7, 8, 9},
                {6, 6, 6, 6, 6, 6, 6, 6, 6},
        };

        ArrayPrinter.printArray(source);

        System.out.println();
        System.out.println("Enter positive number K (number of row)");
        int k = InputReader.nextInt();
        if (k - 1 < 0 && k - 1 >= source.length) {
            System.out.println("Invalid input");
            return;
        }

        System.out.println("Enter positive number S (number of column)");
        int s = InputReader.nextInt();
        if (s - 1 < 0 && s - 1 >= source[0].length) {
            System.out.println("Invalid input");
            return;
        }

        source = deleteRow(source, k);
        source = deleteColumn(source, s);
        ArrayPrinter.printArray(source);
    }

    public static int[][] deleteColumn(int[][] source, int column) {

        for (int j = column - 1; j < source[0].length; j++) {

            for (int i = 0; i < source.length; i++) {

                if (j == source[0].length - 1) {
                    source[i][j] = 0;
                    continue;
                }

                source[i][j] = source[i][j + 1];
            }

        }
        return source;
    }

    public static int[][] deleteRow(int[][] source, int row) {

        for (int i = row - 1; i < source.length; i++) {

            for (int j = 0; j < source[i].length; j++) {

                if (i == source.length - 1) {
                    source[i][j] = 0;
                    continue;
                }
                source[i][j] = source[i + 1][j];
            }
        }

        return source;
    }
}
