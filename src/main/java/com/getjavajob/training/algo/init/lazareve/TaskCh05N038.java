package main.java.com.getjavajob.training.algo.init.lazareve;

public class TaskCh05N038 {

    public static void main(String[] args) {
        System.out.println("Distance from home: " + getDistanceFromHome());
        System.out.println("The duration of the path: " + getDurationOfPath());
    }

    public static double getDistanceFromHome() {
        return Math.log(2);
    }

    public static double getDurationOfPath() {
        return Math.log(100) + 0.5772 + (1 / (2 * 100));
    }
}
