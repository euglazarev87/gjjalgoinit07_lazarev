package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh05N064 {

    public static void main(String[] args) {

        int[][] regionArray = new int[12][2];

        for (int i = 0; i < 12; i++) {
            System.out.print("Please enter area size in km2 for area# " + (i + 1) + ":");
            int area = InputReader.nextInt();
            System.out.println();

            System.out.print("Please enter count of civilian for area# " + (i + 1) + ":");
            int countCivilian = InputReader.nextInt();
            System.out.println();

            int[] details = new int[2];
            details[0] = area;
            details[0] = countCivilian;

            regionArray[i] = details;
        }

        System.out.println("The average population density of this region equals to "
                + getAveragePopulationDensity(regionArray));
    }

    public static double getAveragePopulationDensity(int[][] regionArray) {

        int sumOfArea = 0;
        int sumOfCivilian = 0;
        for (int i = 0; i < regionArray.length; i++) {
            sumOfArea += regionArray[i][0];
            sumOfCivilian += regionArray[i][1];
        }

        return sumOfCivilian / sumOfArea;
    }
}
