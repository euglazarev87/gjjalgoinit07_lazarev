package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh10N042 {

    public static void main(String[] args) {

        System.out.println("Please enter a positive integer for exponentiation: ");
        int naturalDigit = InputReader.nextInt();
        System.out.println("Enter exponent: ");
        int exponent = InputReader.nextInt();

        if (naturalDigit <= 0) {
            System.out.println("Invalid input");
            return;
        }

        System.out.println("Result is " + getPowerOf(naturalDigit, exponent));
    }

    public static int getPowerOf(int naturalDigit, int exponent) {

        if (exponent <= 1) {
            return naturalDigit;
        }

        int res = getPowerOf(naturalDigit, exponent - 1) * naturalDigit;
        return res;
    }
}
