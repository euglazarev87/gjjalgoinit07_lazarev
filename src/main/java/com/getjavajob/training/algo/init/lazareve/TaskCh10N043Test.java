package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N043.*;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N043Test {

    public static void main(String[] args) {
        assertEquals("TaskCh10N043.getSumOfDigits", 6, getSumOfDigits(123));
        assertEquals("TaskCh10N043.getSumOfDigits", 6, getSumOfDigits(321));
        assertEquals("TaskCh10N043.getSumOfDigits", 3, getCountOfDigits(123));
        assertEquals("TaskCh10N043.getSumOfDigits", 5, getCountOfDigits(10000));
    }
}
