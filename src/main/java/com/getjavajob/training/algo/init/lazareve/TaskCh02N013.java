package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh02N013 {

    public static void main(String[] args) {

        System.out.println("Please enter three-digit number");
        int digit = InputReader.nextInt();
        if (digit < 100 || digit > 999) {
            System.out.println("Invalid input");
            return;
        }

        System.out.printf("Inverted number from %d is %d", digit, inverseNumber(digit));
    }

    /*
    * Inverse number value
    * */
    public static int inverseNumber(int value) {
        int result = 0;
        while (value > 0) {
            result = result * 10 + value % 10;
            value /= 10;
        }
        return result;
    }
}
