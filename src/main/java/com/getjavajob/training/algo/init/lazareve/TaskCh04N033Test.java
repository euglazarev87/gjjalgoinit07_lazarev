package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh04N033.*;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N033Test {

    public static void main(String[] args) {
        assertEquals("TaskCh04N033.endsWithEvenNumber", true, endsWithEvenNumber(10));
        assertEquals("TaskCh04N033.endsWithEvenNumber", false, endsWithEvenNumber(33));
        assertEquals("TaskCh04N033.endsWithOddNumber", false, endsWithOddNumber(10));
        assertEquals("TaskCh04N033.endsWithOddNumber", true, endsWithOddNumber(33));
    }
}
