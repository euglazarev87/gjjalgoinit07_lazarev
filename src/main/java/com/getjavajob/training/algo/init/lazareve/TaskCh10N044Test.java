package com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N044.getDigitalRoot;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N044Test {

    public static void main(String[] args) {
        assertEquals("TaskCh10N044.getDigitalRoot", 1, getDigitalRoot(10));
        assertEquals("TaskCh10N044.getDigitalRoot", 3, getDigitalRoot(1569));
    }
}
