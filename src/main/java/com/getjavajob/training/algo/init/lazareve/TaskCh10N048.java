package main.java.com.getjavajob.training.algo.init.lazareve;

public class TaskCh10N048 {

    public static void main(String[] args) {

        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10);
            System.out.println("Create element with index: " + i + " and value: " + array[i]);
        }
        System.out.println();
        System.out.println("Last element of array is " + getLastElement(array));

    }

    public static int getLastElement(int[] arr) {
        return getLastElement(arr, 0);
    }

    private static int getLastElement(int[] arr, int iterator) {

        if (iterator == arr.length - 1) {
            return arr[iterator];
        }

        return getLastElement(arr, iterator + 1);
    }
}
