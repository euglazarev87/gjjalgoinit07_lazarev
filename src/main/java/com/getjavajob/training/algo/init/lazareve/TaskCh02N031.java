package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh02N031 {

    public static void main(String[] args) {

        System.out.println("Please enter an integer that >= 100 and <= 999");
        int userNumber = InputReader.nextInt();

        if (userNumber < 100 || userNumber > 999) {
            System.out.println("Number should be <= 100 and <= 999");
            return;
        }
        System.out.printf("Transformed number from %d equals %d", userNumber, transformNumber(userNumber));
    }

    public static int transformNumber(int number) {

        // find digits of the number by separating integer and fractional parts
        int firstDigit = (number / 100) * 100;
        int secondDigit = number % 10 * 10;
        int thirdDigid = number % 100 / 10;

        return firstDigit + secondDigit + thirdDigid;
    }
}
