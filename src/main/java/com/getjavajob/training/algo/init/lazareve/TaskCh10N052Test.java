package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N052.*;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N052Test {

    public static void main(String[] args) {
        assertEquals("TaskCh10N052.getReversedOutput", "54321", getReversedOutput(12345));
    }
}
