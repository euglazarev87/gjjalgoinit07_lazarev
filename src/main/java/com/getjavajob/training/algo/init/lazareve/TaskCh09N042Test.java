package com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh09N042.getReversedWord;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N042Test {

    public static void main(String[] args) {
        startTest();
    }

    private static void startTest() {
        assertEquals("TaskCh09N042.getReversedWord", "drow", getReversedWord("word"));
        assertEquals("TaskCh09N042.getReversedWord", "avaj", getReversedWord("java"));
    }
}
