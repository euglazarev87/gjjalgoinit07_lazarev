package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh12N234.deleteColumn;
import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh12N234.deleteRow;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N234Test {

    public static void main(String[] args) {

        int[][] deleteRowSource = {
                {1, 2, 3, 4, 5, 6, 7, 8, 9},
                {2, 2, 3, 4, 5, 6, 7, 8, 9},
                {3, 2, 3, 4, 5, 6, 7, 8, 9},
                {4, 2, 3, 4, 5, 6, 7, 8, 9},
                {5, 2, 3, 4, 5, 6, 7, 8, 9},
                {6, 6, 6, 6, 6, 6, 6, 6, 6},
        };

        int[][] deleteRowResult = {
                {2, 2, 3, 4, 5, 6, 7, 8, 9},
                {3, 2, 3, 4, 5, 6, 7, 8, 9},
                {4, 2, 3, 4, 5, 6, 7, 8, 9},
                {5, 2, 3, 4, 5, 6, 7, 8, 9},
                {6, 6, 6, 6, 6, 6, 6, 6, 6},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
        };

        assertEquals("TaskCh12N234.deleteRow", deleteRowResult, deleteRow(deleteRowSource, 1));

        int[][] deleteColumnSource = {
                {1, 2, 3, 4, 5, 6, 7, 8, 9},
                {2, 2, 3, 4, 5, 6, 7, 8, 9},
                {3, 2, 3, 4, 5, 6, 7, 8, 9},
                {4, 2, 3, 4, 5, 6, 7, 8, 9},
                {5, 2, 3, 4, 5, 6, 7, 8, 9},
                {6, 6, 6, 6, 6, 6, 6, 6, 6},
        };

        int[][] deleteColmnResult = {
                {2, 3, 4, 5, 6, 7, 8, 9, 0},
                {2, 3, 4, 5, 6, 7, 8, 9, 0},
                {2, 3, 4, 5, 6, 7, 8, 9, 0},
                {2, 3, 4, 5, 6, 7, 8, 9, 0},
                {2, 3, 4, 5, 6, 7, 8, 9, 0},
                {6, 6, 6, 6, 6, 6, 6, 6, 0},
        };

        assertEquals("TaskCh12N234.deleteColumn", deleteColmnResult, deleteColumn(deleteColumnSource, 1));
    }
}
