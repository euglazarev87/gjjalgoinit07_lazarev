package main.java.com.getjavajob.training.algo.init.lazareve;

public class TaskCh03N029 {

    /**
     * Returns true when x and y are odd
     *
     * @param x int
     * @param y int
     * @return boolean
     */
    public static boolean taskA(int x, int y) {
        return x % 2 > 0 && y % 2 > 0;
    }

    /**
     * Returns true if only one from params < 20
     *
     * @param x int
     * @param y int
     * @return boolean
     */
    public static boolean taskB(int x, int y) {
        return (x < 20 && y >= 20) || (y < 20 && x >= 20);
    }

    /**
     * Returns true if any of params equals to 0
     *
     * @param x
     * @param y
     * @return
     */
    public static boolean taskV(int x, int y) {
        return x == 0 || y == 0;
    }

    /**
     * Returns true if all of params are negative
     *
     * @param x
     * @param y
     * @param z
     * @return
     */
    public static boolean taskG(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    /**
     * Returns true if only one param is a multiple to 5
     *
     * @param x
     * @param y
     * @param z
     * @return
     */
    public static boolean taskD(int x, int y, int z) {
        return (x % 5 == 0 && y % 5 > 0 && z % 5 > 0) ||
                (y % 5 == 0 && x % 5 > 0 && z % 5 > 0) ||
                (z % 5 == 0 && y % 5 > 0 && x % 5 > 0);
    }

    /**
     * Returns true if any of param is greater than 100
     *
     * @param x
     * @param y
     * @param z
     * @return
     */
    public static boolean taskE(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}
