package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh06N008 {

    public static void main(String[] args) {

        System.out.print("Please enter number N: ");
        int n = InputReader.nextInt();
        double[] result = getNotExceedNNumbers(n);

        for (double val : result) {
            System.out.println(val);
        }
    }

    public static double[] getNotExceedNNumbers(int n) {

        int maxNumber = (int) Math.sqrt(n);
        System.out.println("Max number = " + maxNumber);
        double[] result = new double[maxNumber];
        for (int i = 1; i <= maxNumber; i++) {
            result[i - 1] = (int) Math.pow(i, 2);
        }
        return result;
    }
}
