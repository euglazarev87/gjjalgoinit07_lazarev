package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh04N036.currentTrafficLightIsGreen;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N036Test {

    public static void main(String[] args) {
        assertEquals("TaskCh04N036.currentTrafficLightIsGreen", true, currentTrafficLightIsGreen(1));
        assertEquals("TaskCh04N036.currentTrafficLightIsGreen", false, currentTrafficLightIsGreen(4));
    }
}
