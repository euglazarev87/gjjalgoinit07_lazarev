package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N048.getLastElement;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N048Test {

    public static void main(String[] args) {
        int[] arr = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
        assertEquals("TaskCh10N048.getLastElement", 100, getLastElement(arr));
    }
}
