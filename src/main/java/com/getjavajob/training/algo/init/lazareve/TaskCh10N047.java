package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh10N047 {

    public static void main(String[] args) {

        System.out.println("Enter positive integer (K) : ");
        int k = InputReader.nextInt();
        if (k < 1) {
            System.out.println("Invalid input");
            return;
        }

        System.out.println("Element with number K in Fibonacci sequence is " + getElementValue(k));
    }

    /**
     * Returns value of element k in Fibonacci sequence
     * @param k
     * @return
     */
    public static int getElementValue(int k) {

        if (k == 1 || k == 2) {
            return 1;
        }

        return getElementValue(k - 1) + getElementValue(k - 2);
    }
}
