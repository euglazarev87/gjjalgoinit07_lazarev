package main.java.com.getjavajob.training.algo.init.lazareve;

import java.util.Arrays;

public class TaskCh12N023 {

    public static void main(String[] args) {
        printArray(fillArrayA(new int[7][7]));
        System.out.println();

        printArray(fillArrayB(new int[7][7]));
        System.out.println();

        printArray(fillArrayV(new int[7][7]));
        System.out.println();
    }

    private static void printArray(int[][] array) {
        for (int[] one : array) {
            System.out.println(Arrays.toString(one));
        }
    }

    public static int[][] fillArrayA(int[][] source) {

        for (int i = 0; i < source.length; i++) {
            for (int j = 0; j < source[i].length; j++) {

                if ((j == i) || j == ((source[i].length - 1) - i)) {
                    source[i][j] = 1;
                }
            }
        }
        return source;
    }

    public static int[][] fillArrayB(int[][] source) {

        for (int i = 0; i < source.length; i++) {
            for (int j = 0; j < source[i].length; j++) {

                if (j == i || j == ((source[i].length - 1) - i) || i == ((source[i].length - 1) - i)
                        || j == ((source[i].length - 1) - j)) {

                    source[i][j] = 1;
                }
            }
        }

        return source;
    }

    public static int[][] fillArrayV(int[][] source) {

        int n = 0;
        int d = 0;
        for (int i = 0; i < source.length; i++) {
            for (int j = d; j < (source[i].length - d); j++) {
                source[i][j] = 1;
            }
            n++;
            d = (source.length / 2) - Math.abs((source.length / 2) - n);
        }
        return source;
    }

}
