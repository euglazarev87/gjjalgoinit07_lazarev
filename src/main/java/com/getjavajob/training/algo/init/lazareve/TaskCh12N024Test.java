package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.Assert;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh12N024.fillArrayA;
import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh12N024.fillArrayB;

public class TaskCh12N024Test {

    public static void main(String[] args) {

        int[][] arrA = new int[][]{
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}};

        Assert.assertEquals("TaskCh12N024.fillArrayA", arrA, fillArrayA(new int[6][6]));

        int[][] arrB = new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5}};

        Assert.assertEquals("TaskCh12N024.fillArrayB", arrB, fillArrayB(new int[6][6]));
    }
}
