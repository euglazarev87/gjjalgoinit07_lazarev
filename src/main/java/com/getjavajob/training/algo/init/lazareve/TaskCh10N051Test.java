package main.java.com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N051.solveAlgorithmA;
import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N051.solveAlgorithmB;
import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh10N051.solveAlgorithmV;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N051Test {

    public static void main(String[] args) {
        assertEquals("TaskCh10N051.solveAlgorithmA", "54321", solveAlgorithmA(5));
        assertEquals("TaskCh10N051.solveAlgorithmB", "12345", solveAlgorithmB(5));
        assertEquals("TaskCh10N051.solveAlgorithmV", "5432112345", solveAlgorithmV(5));
    }
}
