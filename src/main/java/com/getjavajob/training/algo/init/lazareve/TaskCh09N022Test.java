package com.getjavajob.training.algo.init.lazareve;

import static main.java.com.getjavajob.training.algo.init.lazareve.TaskCh09N022.getFirstHalfOfTheWord;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N022Test {

    public static void main(String[] args) {
        startTest();
    }

    private static void startTest() {
        assertEquals("TaskCh09N022.getFirstHalfOfTheWord", "half", getFirstHalfOfTheWord("halfhalf"));
        assertEquals("TaskCh09N022.getFirstHalfOfTheWord", "", getFirstHalfOfTheWord(""));
        assertEquals("TaskCh09N022.getFirstHalfOfTheWord", "th", getFirstHalfOfTheWord("three"));
    }
}
