package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh10N041 {

    public static void main(String[] args) {

        System.out.println("Please enter a positive integer: ");
        int n = InputReader.nextInt();
        System.out.println("Factorial of " + n + " is " + getFactorial(n));
    }

    public static int getFactorial(int n) {

        if (n == 1) {
            return n;
        }

        return getFactorial(n - 1) * n;
    }
}
