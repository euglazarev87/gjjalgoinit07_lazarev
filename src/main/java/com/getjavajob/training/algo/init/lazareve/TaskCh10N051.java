package main.java.com.getjavajob.training.algo.init.lazareve;

public class TaskCh10N051 {

    public static void main(String[] args) {

        // hardcoded value from task
        int n = 5;

        System.out.println("First task named A - " + solveAlgorithmA(n));
        System.out.println("Second task named B - " + solveAlgorithmB(n));
        System.out.println("Third task named V - " + solveAlgorithmV(n));
    }

    public static String solveAlgorithmA(int n) {
        return solveAlgorithmA(n, new StringBuilder());
    }

    private static String solveAlgorithmA(int n, StringBuilder sb) {

        if (n > 0) {
            sb.append(n);
            solveAlgorithmA(n - 1, sb);
        }

        return sb.toString();
    }

    public static String solveAlgorithmB(int n) {
        return solveAlgorithmB(n, new StringBuilder());
    }

    private static String solveAlgorithmB(int n, StringBuilder sb) {

        if (n > 0) {
            solveAlgorithmB(n - 1, sb);
            sb.append(n);
            return sb.toString();
        }
        return "";
    }

    public static String solveAlgorithmV(int n) {
        return solveAlgorithmV(n, new StringBuilder());
    }

    private static String solveAlgorithmV(int n, StringBuilder sb) {

        if (n > 0) {
            sb.append(n);
            solveAlgorithmV(n - 1, sb);
            sb.append(n);
        }
        return sb.toString();
    }

}
