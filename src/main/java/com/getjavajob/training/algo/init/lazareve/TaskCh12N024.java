package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.ArrayPrinter;

public class TaskCh12N024 {

    public static void main(String[] args) {

        int[][] arrA = new int[6][6];
        ArrayPrinter.printArray(fillArrayA(arrA));
        System.out.println();
        int[][] arrB = new int[6][6];
        ArrayPrinter.printArray(fillArrayB(arrB));
    }

    public static int[][] fillArrayA(int[][] arr) {

        int topCell;
        int leftCell;
        for (int i = 0; i < arr.length; i++) {

            for (int j = 0; j < arr[i].length; j++) {

                if (i == 0) {
                    arr[i][j] = 1;
                    continue;
                }

                topCell = i == 0 ? 1 : arr[i - 1][j];
                leftCell = j == 0 ? 0 : arr[i][j - 1];

                arr[i][j] = topCell + leftCell;
            }

        }
        return arr;
    }

    public static int[][] fillArrayB(int[][] arr) {

        for (int i = 0; i < arr.length; i++) {

            int maxCount = arr[i].length;
            int value = i + 1;
            for (int j = 0; j < maxCount; j++) {
                if (value > maxCount) {
                    value = 1;
                }

                arr[i][j] = value;
                value++;
            }

        }
        return arr;
    }
}
