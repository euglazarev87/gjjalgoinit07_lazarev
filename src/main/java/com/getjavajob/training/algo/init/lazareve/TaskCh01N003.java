package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.InputReader;

public class TaskCh01N003 {

    public static void main(String[] args) {

        int userInput = InputReader.nextInt();
        System.out.println("You have entered a number " + userInput);
    }

}
