package main.java.com.getjavajob.training.algo.init.lazareve;

import main.java.com.getjavajob.training.algo.util.ArrayPrinter;

public class TaskCh11N245 {

    public static void main(String[] args) {
        int[] source = {1, 0, 2, 3, 4, 5, 6, 7, -1, -5, -3, -2, 9, 10};
        ArrayPrinter.printArray(getNegativeSortedArray(source));
    }

    /**
     * Returns array with negative first elements
     * @param source - source array
     * @return
     */
    public static int[] getNegativeSortedArray(int[] source) {

        int[] result = new int[source.length];
        int lastIndex = 0;
        for (int i = 0; i < source.length; i++) {
            if (source[i] >= 0) {
                result[lastIndex] = source[i];
            } else {

                for (int j = result.length - 1; j >= 1; j--) {
                    result[j] = result[j - 1];
                }
                result[0] = source[i];
            }
            lastIndex++;
        }

        return result;
    }
}
