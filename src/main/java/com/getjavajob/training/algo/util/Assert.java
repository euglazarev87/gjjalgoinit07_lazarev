package main.java.com.getjavajob.training.algo.util;

import java.util.Arrays;

public class Assert {

    public static void assertEquals(String methodName, boolean expected, boolean actual) {

        if (expected == actual) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String methodName, int expected, int actual) {
        if (expected == actual) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String methodName, double expected, double actual) {
        if (expected == actual) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String methodName, String expected, String actual) {
        if (expected.equals(actual)) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String methodName, char expected, char actual) {
        if (expected == actual) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String methodName, double[] expected, double[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String methodName, int[] expected, int[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String methodName, String[] expected, String[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String methodName, int[][] expected, int[][] actual) {
        if (Arrays.deepEquals(expected, actual)) {
            System.out.println(methodName + " Success");
        } else {
            System.out.println(methodName + " Failure: expected " + Arrays.deepToString(expected) + ", actual " + Arrays.deepToString(actual));
        }
    }

}
