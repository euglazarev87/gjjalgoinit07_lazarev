package main.java.com.getjavajob.training.algo.util;

import java.util.Arrays;

public class ArrayPrinter {

    public static void printArray(int[][] array) {
        for (int[] arr : array) {
            System.out.println(Arrays.toString(arr));
        }
    }

    public static void printArray(String[] array) {
        System.out.println(Arrays.toString(array));
    }

    public static void printArray(int[] array) {
        System.out.println(Arrays.toString(array));
    }
}
