package main.java.com.getjavajob.training.algo.util;

import java.util.Scanner;

public class InputReader {

    private static Scanner instance;

    private InputReader() {
    }

    public static int nextInt() {
        return getInstance().nextInt();
    }

    public static String nextLine() {
        return getInstance().nextLine();
    }

    public static String next() {
        return getInstance().next();
    }

    private static Scanner getInstance() {

        if (instance == null) {
            instance = new Scanner(System.in);
        }
        return instance;
    }

}
